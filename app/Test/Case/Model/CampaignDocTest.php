<?php
App::uses('CampaignDoc', 'Model');

/**
 * CampaignDoc Test Case
 */
class CampaignDocTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.campaign_doc',
		'app.doc_type',
		'app.campaign'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CampaignDoc = ClassRegistry::init('CampaignDoc');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CampaignDoc);

		parent::tearDown();
	}

}
