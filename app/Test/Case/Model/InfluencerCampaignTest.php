<?php
App::uses('InfluencerCampaign', 'Model');

/**
 * InfluencerCampaign Test Case
 */
class InfluencerCampaignTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.influencer_campaign',
		'app.influencer',
		'app.campaign',
		'app.agency',
		'app.user',
		'app.notification',
		'app.campaign_status',
		'app.campaign_doc',
		'app.doc_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->InfluencerCampaign = ClassRegistry::init('InfluencerCampaign');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->InfluencerCampaign);

		parent::tearDown();
	}

}
