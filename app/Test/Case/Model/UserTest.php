<?php
App::uses('User', 'Model');

/**
 * User Test Case
 */
class UserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user',
		'app.agency',
		'app.campaign',
		'app.campaign_status',
		'app.influencer_campaign',
		'app.influencer',
		'app.influencer_bank_detail',
		'app.influencer_category',
		'app.category',
		'app.social_profile',
		'app.social_network',
		'app.campaign_doc',
		'app.doc_type',
		'app.notification',
		'app.notification_status',
		'app.notification_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->User = ClassRegistry::init('User');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->User);

		parent::tearDown();
	}

}
