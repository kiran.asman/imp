<?php
App::uses('Notification', 'Model');

/**
 * Notification Test Case
 */
class NotificationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.notification',
		'app.agency',
		'app.user',
		'app.campaign',
		'app.campaign_status',
		'app.influencer_campaign',
		'app.influencer',
		'app.influencer_bank_detail',
		'app.influencer_category',
		'app.category',
		'app.social_profile',
		'app.campaign_doc',
		'app.doc_type',
		'app.notification_status',
		'app.notification_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Notification = ClassRegistry::init('Notification');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Notification);

		parent::tearDown();
	}

}
