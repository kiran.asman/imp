<?php
/**
 * Campaign Fixture
 */
class CampaignFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'agency_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'campaign_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 350, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'brand_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 350, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'hash_tags' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 350, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'content_guidelines' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'sample_content' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'last_date_to_apply' => array('type' => 'date', 'null' => true, 'default' => null),
		'start_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'submission_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'is_active' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'campaign_status_id' => array('type' => 'integer', 'null' => false, 'default' => '3', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'agency_id' => '',
			'campaign_name' => 'Lorem ipsum dolor sit amet',
			'brand_name' => 'Lorem ipsum dolor sit amet',
			'hash_tags' => 'Lorem ipsum dolor sit amet',
			'content_guidelines' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'sample_content' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'last_date_to_apply' => '2018-12-04',
			'start_date' => '2018-12-04',
			'submission_date' => '2018-12-04',
			'is_active' => 1,
			'created' => '2018-12-04 06:33:12',
			'modified' => '2018-12-04 06:33:12',
			'campaign_status_id' => 1
		),
	);

}
