<!--
<?php

	//$xml = Xml::fromArray(array("status","message","content", "pagination"), array('format' =>'tags'));
	//$xml = $xml->asXML();
	//echo $xml;
?>

<?php $Xml->serialize(array("status","message","content", "pagination")); ?>
-->

<?php


 /**
 *
 * View Class for XML
 *
 * @company 	Fingersfun 
 * @link 		www.fingersfun.com 
 * @version 	Version 0.1.0 
 * @developer 	Kiran Kumar.M (kiran@fingersfun.com) 
 *
 * */
class XmlView extends View {
	public $BluntXml;
	public function render ($action = null, $layout = null, $file = null) {
		if (!array_key_exists('response', $this->viewVars)) {
			trigger_error(
				'viewVar "response" should have been set by Rest component already',
				E_USER_ERROR
			);
			return false;
		}

		return $this->encode($this->viewVars['response']);
	}

	public function headers ($Controller, $settings) {
		if ($settings['debug'] > 2) {
			return null;
		}

		header('Content-Type: text/xml');
		$Controller->RequestHandler->setContent('xml', 'text/xml');
		$Controller->RequestHandler->respondAs('xml');
		return true;
	}

	public function encode ($response) {
		require_once dirname(dirname(__FILE__)) . '/Libs/BluntXml.php';
		$this->BluntXml = new BluntXml();
		return $this->BluntXml->encode(
			$response,
			Inflector::tableize($this->params['controller']) . '_response'
		);
	}
}