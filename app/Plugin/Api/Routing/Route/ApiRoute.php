<?php

App::uses('CakeRoute', 'Routing/Route');

/**
 * Description of ApiRoute
 *
 * @author Kiran Kumar M
 */
class ApiRoute extends CakeRoute {

/**
 * parse
 *
 * @param $url
 * @return
 */
    function parse($url) {
        if (preg_match('#^/' . $this->defaults['prefix'] . '/([^/:]+)/(.+)#', $url, $matches)) {
            $c = $this->defaults['prefix'] . '/' . $matches[1];
            if (preg_match('#^/' . $c . '(/?)([^/]*)/?(.*)#', $url, $matches)) {
                if ($matches[1] === '') {
                    $url .= '/';
                }
                if ($matches[2] === '') {
                    $url .= 'index';
                }
                if (strpos($matches[2], ':')) {
                    unset($matches[0]);
                    unset($matches[1]);
                    $url = '/' . $c . '/index/' . implode('/', $matches);
                }
            }
        } else if (preg_match('#^/' . $this->defaults['prefix'] . '/([^/:]+)/?$#', $url, $matches)) {
            $url = '/' . $this->defaults['prefix'] . '/' . $matches[1] . '/index';
        }
        $route = parent::parse($url);        
        //pr(App::paths());
        if (is_array($route)) {
            //$route['controller'] = $this->defaults['prefix']."/".$this->defaults['prefix'] . '_' . $route['controller'];
            //$route['controller'] = $this->defaults['prefix'] . '_' . $route['controller'];
            $route['action'] = str_replace('.', '_', $route['version']) . '_' . $route['action'];
        }
        return $route;
    }

/**
 * match
 *
 * @param $url
 * @return
 */
    function match($url) {
        
        if (preg_match('/^' . $this->defaults['prefix'] . '_/', $url['controller'])) {
            // backup defaults
            $defaultsBackup = $this->defaults;
            // remove controllerPrefix from controller
            $url['controller'] = preg_replace('/^' . $this->defaults['prefix'] . '_/', '', $url['controller']);
            $this->default['prefix'] = $this->defaults['prefix'];
            unset($this->defaults['prefix']);
            $result = parent::match($url);
            // restore defaults
            $this->defaults = $defaultsBackup;
            //return $result;
        }
    }

}
