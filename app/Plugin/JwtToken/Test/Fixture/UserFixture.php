<?php
class UserFixture extends CakeTestFixture {

    /**
     * name property
     *
     * @var string 'User'
     */
    public $name = 'User';

    /**
     * fields property
     *
     * @var array
     */
    public $fields = array(
        'id' => array('type' => 'integer', 'key' => 'primary'),
        'name' => array('type' => 'string', 'null' => false),
        'email' => array('type' => 'string', 'null' => false),
        'password' => array('type' => 'string', 'null' => false),
        'token' => array('type' => 'string', 'null' => false),
        'created' => 'datetime',
        'updated' => 'datetime'
    );

    /**
     * records property
     *
     * @var array
     */
    public $records = array(
        array('name' => 'kiran', 'email' => 'kiran@example.com', 'password' => '55d61d8327deb882cf99f4dcc3b5aa76', 'token' => '54321', 'created' => '2017-10-07 14:05:00', 'updated' => '2017-10-07 14:05:00'),
        array('name' => 'kintu', 'email' => 'kintu@example.com', 'password' => '55d61d8327deb882cf99f4dcc3b5aa76', 'token' => '12345', 'created' => '2017-10-07 14:05:00', 'updated' => '2017-10-07 14:05:00'),
        array('name' => 'sam', 'email' => 'sam@example.com', 'password' => '55d61d8327deb882cf99f4dcc3b5aa76', 'token' => '98567', 'created' => '2017-10-07 14:05:00', 'updated' => '2017-10-07 14:05:00'),
        array('name' => 'funny', 'email' => 'funny@example.com', 'password' => '55d61d8327deb882cf99f4dcc3b5aa76', 'token' => '32151', 'created' => '2017-10-07 14:05:00', 'updated' => '2017-10-07 14:05:00'),
        array('name' => 'anu', 'email' => 'anu@example.com', 'password' => '55d61d8327deb882cf99f4dcc3b5aa76', 'token' => '98345', 'created' => '2017-10-07 14:05:00', 'updated' => '2017-10-07 14:05:00'),

    );
}
