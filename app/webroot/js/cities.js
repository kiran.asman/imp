(function(){	
	var app = angular.module('app');

	app.controller('CitiesController', function($scope, $route, $routeParams, $location) {
		$scope.$route = $route;
		$scope.$location = $location;
		$scope.$routeParams = $routeParams;
	});

	app.controller("CitiesIndexController", function($scope, $http, components) {
		$scope.predicate = 'id';
		$scope.name = "CitiesIndexController";
		$scope.cities = [];
		$http.get('//http://api.localhost/cakeadmintest/cities/').success(function(data) {
	        $scope.cities = data;
			components.paginate($scope, data);
	    });
	});

	app.controller("CitiesViewController", function($scope, $http, $routeParams) {
		$scope.name = "CitiesViewController";
		$scope.cities = [];
		$http.get('//http://api.localhost/cakeadmintest/cities/' + $routeParams.id).success(function(data) {
	        $scope.cities = data;
	    });
	});

	app.controller("CitiesAddController", function($scope, $http, $routeParams, $location) {
		$scope.name = "CitiesAddController";
		$scope.countries = [];
		$http.get('//http://api.localhost/cakeadmintest/countries/').success(function(data) {
	        $scope.countries = data;
	    });
		$scope.send = function(){
            $http({
            	url: '//http://api.localhost/cakeadmintest/cities/',
            	method: "POST",
            	data: 'body=' + JSON.stringify({City:$scope.cities.City}),
            	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    		}).success(function (data, status, headers, config) {
    			if(data.City.type = 'success'){
    				$location.path("/cakeadmintest");
    			} else {
    				alert('City could not be added.')
    			}
        	}).error(function (data, status, headers, config) {
	        	alert(status + ' ' + data);
            });
		}
	});

	app.controller("CitiesEditController", function($scope, $http, $routeParams, $location) {
		$scope.name = "CitiesEditController";
		$scope.countries = [];
		$http.get('//http://api.localhost/cakeadmintest/countries/').success(function(data) {
	        $scope.countries = data;
	    });
		$scope.cities = [];
		$http.get('//http://api.localhost/cakeadmintest/cities/' + $routeParams.id).success(function(data) {
	        $scope.cities = data;
	    });
		$scope.send = function(){
            $http({
            	url: '//http://api.localhost/cakeadmintest/cities/' + $routeParams.id,
            	method: "PUT",
            	data: 'body=' + JSON.stringify({City:$scope.cities.City}),
            	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    		}).success(function (data, status, headers, config) {
    			if(data.City.type = 'success'){
    				$location.path("/cakeadmintest");
    			} else {
    				alert('City could not be updated.')
    			}
        	}).error(function (data, status, headers, config) {
	        	alert(status + ' ' + data);
            });
		}
	});

	app.config(function($routeProvider) {
		$routeProvider
			.when('/cakeadmintest', {
				templateUrl : '/view/cities/index.html',
				controller  : 'CitiesIndexController'
			})
			.when('/cakeadmintest/view/:id', {
				templateUrl : '/view/cities/view.html',
				controller  : 'CitiesViewController'
			})
			.when('/cakeadmintest/add', {
				templateUrl : '/view/cities/add.html',
				controller  : 'CitiesAddController'
			})
			.when('/cakeadmintest/edit/:id', {
				templateUrl : '/view/cities/edit.html',
				controller  : 'CitiesEditController'
			})
			.when('/cakeadmintest/delete/:id', {
		        resolve: {
		            citiesDelete: function ($http, $route, $location) {
		               $http.delete('//http://api.localhost/cakeadmintest/cities/'+ $route.current.params.id)
						.success(function(data) {
							if(data.City.type = 'success'){
								$location.path("/cakeadmintest");
							} else {
								alert('City could not be deleted.')
							}
						}).error(function (data, status, headers, config) {
							alert('City could not be deleted.')
						});
		            }
		       }
			})
	});

})();