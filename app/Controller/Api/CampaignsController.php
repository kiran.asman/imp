<?php

App::uses('ApiController', 'Api.Controller');

class CampaignsController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * @method createCampaign
     * 
     * URL: http://192.168.0.22/mvp/api/1.0/json/campaigns/createCampaign/
     * REQUEST :
     * 
     *  {
     *      
     *      "agency_id": "11"
     *  }
     * METHOD : POST    
     * 
     * RESPONSE SUCCESS:
     *  {
      "status": "Success",
      "message": "Campaign Registered successfully.",
      "content": {
      "agency_id": "11",
      "campaign_name": "Iphone10x",
      "brand_name": "Apple",
      "hash_tags": "#Apple#iphone",
      "submission_date": "2018-12-15",
      "campaign_id": "16"
      }
      }
     * 
     */
    public function api_1_0_createCampaign() {

        if ($this->request->is('post')) {
            $this->layout = 'json';
            $requesteddata = $this->request->data;
            $agency_id = $requesteddata['agency_id'];
            //$agency_id = $requesteddata['agency_id'];
            if (!empty($agency_id)) {

                //$requesteddata['agency_id']=$agency_id;
                if ($this->Campaign->Save($requesteddata)) {
                    $this->loadModel('Campaign');
                    //$requesteddata['campaign_id']=$this->Campaign->id;
                    //$this->loadModel('CampaignDoc');
                    //$this->CampaignDoc->Save($requesteddata); 				
                    $status = 'Success';
                    $message = 'Campaign craeted successfully.';
                    $content = $requesteddata;
                } else {
                    $status = 'Failed';
                    $message = 'Campaign could not be craeted, please try again.';
                    $content = $requesteddata;
                }
            } else {
                $status = 'Failed';
                $message = 'Invalid agency.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /**
     * @method updateCamapign
     * 
     * URL: http://192.168.0.22/mvp/api/1.0/json/campaigns/updateCampaign/
     * REQUEST :
     * 
     *  {
     *      "id":16,
     *       "campaign_name":"Sports",
     *       "brand_name":"Sports description"
     *   }  
     *     
     *  
     * METHOD : PUT    
     * 
     * RESPONSE SUCCESS:
     * {
      "status": "SUCCESS",
      "message": "Campaign updated.",
      "content": {
      "id": 10,
      "category": "Sports",
      "description": "Sports description"
      }
      }
     * 
     */
    public function api_1_0_updateCampaign() {

        if ($this->request->is('put')) {
            $this->layout = 'json';
            $requesteddata = $this->request->data;
            $isCampaignExist = $this->Campaign->find('first', array('conditions' => array(
                    'Campaign.id' => $requesteddata['id']
            )));
            if (!empty($isCampaignExist)) {

                $this->Campaign->id = $isCampaignExist['Campaign']['id'];
                $this->loadModel('Campaign');
                if ($this->Campaign->Save($requesteddata)) {

                    $status = 'SUCCESS';
                    $message = 'Campaign updated.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Campaign update failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'Failed';
                $message = 'Campaign does not exist.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /**
     * @method getCamapigns
     * 
     * URL: http://192.168.0.22/mvp/api/1.0/json/campaigns/getCampaigns/user_id:1/member_id:2
     * REQUEST :
     * 
     * METHOD : GET, POST
     *      {
     *          "user_id":1
     *     }
     * RESPONSE SUCCESS:
      {
      "status": "SUCCESS",
      "message": "Records found",
      "content":{
      "id": "10",
      "campaign_name": "Samsung J7Pro Promotion",
      "brand_name": "Samsung J7",
      "last_date_to_apply": "2018-12-04"
      }

      }
     */
    public function api_1_0_getCampaigns() {
        /*
        if(!empty($this->getTokenInfo())){
            $requesteddata = $this->getTokenInfo();            
        }
        
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
        }
        if ($this->request->is('get')) {
            $requesteddata = $this->request->params['named'];
        }
        */
        $content = null;
        $this->paginate = array(
            'limit' => 10,
            'fields' => array(
                'id',
                'campaign_name',
                'brand_name',
                'last_date_to_apply',
            // 'description'
            ),
            'conditions' => array(
                'Campaign.is_active' => 1,
            ),
            'recursive' => -1,
                //'order' => array('Influencer.id' => 'desc')
        );
        $this->loadModel('Campaign');
        $campaigns = $this->paginate('Campaign');
        $campaigns = Set::extract('/Campaign/.', $campaigns);
        if ($campaigns) {
            $message = 'Records found';
            $status = 'SUCCESS';
            $content = $campaigns;
        } else {
            $status = 'SUCCESS';
            $message = 'Records not found';
        }
        $pagination = $this->request->params['paging']['Campaign'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /* 	URL: http://192.168.0.22/mvp/api/1.0/json/campaigns/getCampaigns/   */

    public function api_1_0_changeCampaignStatus() {


        if ($this->request->is('put')) {
            $this->layout = 'json';
            $requesteddata = $this->request->data;
            $this->loadModel('Campaign');
            $isCampaignExist = $this->Campaign->find('first', array('conditions' => array(
                    'Campaign.id' => $requesteddata['campaign_id']
            )));

            if (!empty($isCampaignExist)) {

                $this->loadModel('CampaignStatus');
                $campaignStatus = $this->CampaignStatus->find('first', array('conditions' => array(
                        'CampaignStatus.status_code' => $requesteddata['status_code']
                )));

                $requesteddata['campaign_status_id'] = $campaignStatus['CampaignStatus']['id'];

                $this->Campaign->id = $isCampaignExist['Campaign']['id'];

                if ($this->Campaign->Save($requesteddata)) {

                    $status = 'SUCCESS';
                    $message = 'Campaign Status updated.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Campaign Status update failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'Failed';
                $message = 'Campaign does not exist.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
