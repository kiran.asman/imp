<?php

App::uses('ApiController', 'Api.Controller');


class SocialNetworksController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    /**
     * @method getSocialNetworks
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/social_networks/getSocialNetworks/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * 
     * RESPONSE SUCCESS:
     * {
        "status": "SUCCESS",
        "message": "Social networks found",
        "content": [
          {
            "id": "9",
            "social_network_name": "Blog",
            "social_network_name_code": "BLOG",
            "social_network_secret_key": "BLOG_KEY",
            "social_networkid": "BLOG_ID"
          },
          {
            "id": "8",
            "social_network_name": "Zomato",
            "social_network_name_code": "ZOMATO",
            "social_network_secret_key": "ZOMATO_KEY",
            "social_networkid": "ZOMATO_ID"
          }
     *    ....
     *    ....
        ],
        "pagination": {
          "page": 1,
          "current": 9,
          "count": 9,
          "prevPage": false,
          "nextPage": false,
          "pageCount": 1,
          "limit": 10,
          "paramType": "named"
        }
      }
     * 
     * 
     */
    
    public function api_1_0_getSocialNetworks() {
        
        $this->paginate = array(
            'limit' => 10, 
              'fields' => array(
              'id',
              'social_network_name',
              'social_network_code',
              'social_network_secret_key',
              'social_networkid'
                  ), 
            'conditions' => array(
                'SocialNetwork.is_active' => ACTIVE,
                ),
            'recursive' => -1,
            'order' => array('SocialNetwork.id' => 'desc')
        );
        $this->loadModel('SocialNetwork');
        $sntwrks = $this->paginate();
        $social_network = Set::extract('/SocialNetwork/.', $sntwrks);
        if ($social_network) {
            $message = 'Social networks found';
            $status = 'SUCCESS';
            $content = $social_network;
        } else {
            $status = 'SUCCESS';
            $message = 'Social networks not found';
        }
        $pagination = $this->request->params['paging']['SocialNetwork'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    
    /**
     * @method createSocialNetwork
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/social_networks/createSocialNetwork/
     * REQUEST :
     * 
     *  {
     *      'social_network_name',
            'social_network_name_code',
            'social_network_secret_key',
            'social_networkid'
     *  }
     * METHOD : POST 
     * 
     * * RESPONSE SUCCESS:
     *  {
            "status": "SUCCESS",
            "message": "Social network created.",
            "content": {
              "id": 10,
              "social_network_name": "Demo",
              "social_network_name_code": "Demo code",
              "social_network_secret_key":"demo key"
              "social_networkid":"demo app id"
            }
        }   
     * 
     */
    public function api_1_0_createSocialNetwork() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;            
            
            $this->SocialNetwork->set($requesteddata);
            if($this->SocialNetwork->validates()){
                $errors = false;
                if ($this->SocialNetwork->save($requesteddata)) {
                    $status = 'SUCCESS';
                    $message = 'Social network created.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Social network creation failed';
                    $content = $requesteddata;
                }
            }else{
                $status = 'ERROR';
                $message = 'Data validation error.';
                $content = $requesteddata;
                $errors = $this->SocialNetwork->validationErrors;
            }
            /*
            if ($this->SocialNetwork->save($requesteddata)) {
                $status = 'SUCCESS';
                $message = 'Social network created.';
                $content = $requesteddata;
            } else {
                $status = 'ERROR';
                $message = 'Social network creation failed';
                $content = $requesteddata;
            }
            */
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /**
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/social_networks/socialNetworkDetails/
     * REQUEST :
     * 
     *  {
     *      "id":10
     *  }
     * METHOD : POST
     * 
     * ====================================================================================
     * 
     *  URL: http://localhost:90/impapi/api/1.0/json/social_networks/socialNetworkDetails/id:10
     * 
     *  METHOD : GET
     * 
     * RESONSE SUCCESS:
     * 
     * {
        "status": "SUCCESS",
        "message": "Social network Information found",
        "content": {
            "id":10,
            "social_network_name": "Demo",
            "social_network_name_code": "Demo code",
            "social_network_secret_key":"demo key"
            "social_networkid":"demo app id"
        }
      }
     * 
     */
    public function api_1_0_socialNetworkDetails() {
        if ($this->request->is(array('post','get'))) {
            if ($this->request->is('post')) {
                $requesteddata = $this->request->data;
            }
            if ($this->request->is('get')) {
                $requesteddata = $this->request->params['named'];
            }
            if ($requesteddata['id']) {
                $social_network_info = $this->SocialNetwork->find('first', array(
                    'fields' => array('id','social_network_name',
              'social_network_code',
              'social_network_secret_key',
              'social_networkid'
                    ), 
                    'conditions' => array(
                        'SocialNetwork.is_active' => ACTIVE, 
                        'SocialNetwork.id' => $requesteddata['id']),
                        'recursive' => -1
                        )
                );
                //print_r($social_network_info);
                if (!empty($social_network_info)) {
                    $status = 'SUCCESS';
                    $message = 'Social network Information found';
                    $content = $social_network_info['SocialNetwork'];
                } else {
                    $status = 'ERROR';
                    $message = 'Invaled social network';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invaled Data';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    /**
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/social_networks/updateSocialNetwork/
     * REQUEST :
     * 
     *  {
     *      "id":10,
            "social_network_name": "Demo",
            "social_network_name_code": "Demo code",
            "social_network_secret_key":"demo key"
            "social_networkid":"demo app id"
     *  }
     * METHOD : PUT    
     * 
     * RESPONSE SUCCESS:
     * {
        "status": "SUCCESS",
        "message": "Social network updated.",
        "content": {
            "id": 10,
            "social_network_name": "Demo",
            "social_network_name_code": "Demo code",
            "social_network_secret_key":"demo key"
            "social_networkid":"demo app id"
        }
      }
     * 
     * 
     */
    public function api_1_0_updateSocialNetwork() {
        if ($this->request->is('put')) {
            $requesteddata = $this->request->data;
            $isSocialNetworkExist = $this->SocialNetwork->find('first', array('conditions' => array(
                    'SocialNetwork.id' => $requesteddata['id']
            )));
            if (!empty($isSocialNetworkExist)) {
                $this->SocialNetwork->id = $isSocialNetworkExist['SocialNetwork']['id'];
                $this->SocialNetwork->set($requesteddata);
                if($this->SocialNetwork->validates()){
                    if ($this->SocialNetwork->save($requesteddata)) {
                        $status = 'SUCCESS';
                        $message = 'Social network updated.';
                        $content = $requesteddata;
                    } else {
                        $status = 'ERROR';
                        $message = 'Social network update failed';
                        $content = $requesteddata;
                    }
                }else{
                    $status = 'ERROR';
                    $message = 'Data validation error.';
                    $content = $requesteddata;
                    $errors = $this->SocialNetwork->validationErrors;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invaled Category';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    
    /**
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/social_networks/deleteSocialNetwork/
     * REQUEST :
     * 
     *  {
     *      "id":10,
     *  }
     * METHOD : DELETE    
     * 
     * RESPONSE SUCCESS:
     * 
     * {
        "status": "SUCCESS",
        "message": "Social network deleted.",
        "content": {
          "id": 10
        }
      }
     * 
     */
    public function api_1_0_deleteSocialNetwork() {
        if ($this->request->is('delete')) {
            $requesteddata = $this->request->data;
            $isSocialNetworkExist = $this->SocialNetwork->find('first', array('conditions' => array(
                    'SocialNetwork.id' => $requesteddata['id']
            )));
            if (!empty($isSocialNetworkExist)) {
                $this->SocialNetwork->id = $isSocialNetworkExist['SocialNetwork']['id'];
                if ($this->SocialNetwork->saveField('is_active',INACTIVE)) {
                    $status = 'SUCCESS';
                    $message = 'Social network deleted.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Social network delete failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invaled Social network';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
