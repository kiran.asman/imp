<?php

App::uses('ApiController', 'Api.Controller');


class CategoriesController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    /**
     * @method getCategories
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/categories/getCategories/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * RESPONSE SUCCESS :
     * {
            "status": "SUCCESS",
            "message": "Categories found",
            "content": [
                {
                    "id": "14",
                    "category": "General"
                },
                {
                    "id": "13",
                    "category": "Games"
                }
     *          ...
     *          ...
     * 
            ],
            "pagination": {
                "page": 1,
                "current": 10,
                "count": 14,
                "prevPage": false,
                "nextPage": true,
                "pageCount": 2,
                "limit": 10,
                "paramType": "named"
            }
        }
     * 
     */
    
    public function api_1_0_getCategories() {
        
        $this->paginate = array(
            'limit' => 10, 
              'fields' => array(
              'id',
              'category',
             // 'description'
                  ), 
            'conditions' => array(
                'Category.is_active' => ACTIVE,
                ),
            'recursive' => -1,
            'order' => array('Category.id' => 'desc')
        );
        $this->loadModel('Category');
        $categoris = $this->paginate();
        $categories = Set::extract('/Category/.', $categoris);
        if ($categories) {
            $message = 'Categories found';
            $status = 'SUCCESS';
            $content = $categories;
        } else {
            $status = 'SUCCESS';
            $message = 'Categories not found';
        }
        $pagination = $this->request->params['paging']['Category'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    
    /**
     * @method createCategory
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/categories/createCategory/
     * REQUEST :
     * 
     *  {
     *      "category":"Demo",
     *      "description":"Demo description.."
     *  }
     * METHOD : POST    
     * 
     * RESPONSE SUCCESS:
     *  {
            "status": "SUCCESS",
            "message": "Category created.",
            "content": {
              "id": 10,
              "category": "Demo",
              "description": "Demo description.."
            }
        }
     * 
     */
    public function api_1_0_createCategory() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;            
            $this->Category->set($requesteddata);
            if($this->Category->validates()){
                $errors = false;
                if ($this->Category->save($requesteddata)) {
                    $status = 'SUCCESS';
                    $message = 'Category created.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Category creation failed';
                    $content = $requesteddata;
                }
            }else{
                $status = 'ERROR';
                $message = 'Data validation error.';
                $content = $requesteddata;
                $errors = $this->Category->validationErrors;
            }
            /*
            if ($this->Category->save($requesteddata)) {
                $status = 'SUCCESS';
                $message = 'Category created.';
                $content = $requesteddata;
            } else {
                $status = 'ERROR';
                $message = 'Category creation failed';
                $content = $requesteddata;
            }
            */
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        if($errors){
            $this->set([
                'status' => $status,
                'message' => $message,
                'content' => $content,
                'errors' => $errors,
                '_serialize' => ['status', 'message', 'content','errors']
            ]);
        }else{
            $this->set([
                'status' => $status,
                'message' => $message,
                'content' => $content,
                '_serialize' => ['status', 'message', 'content']
            ]);
        }
        $this->render('/' . $this->request->params['ext']);
    }

    /**
     * @method categoryDetails
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/categories/categoryDetails/
     * REQUEST :
     * 
     *  {
     *      "id":10
     *  }
     * METHOD : POST
     * 
     * ====================================================================================
     * 
     *  URL: http://localhost:90/impapi/api/1.0/json/categories/categoryDetails/id:10
     * 
     *  METHOD : GET
     * 
     * RESONSE SUCCESS:
     * 
     * {
        "status": "SUCCESS",
        "message": "Category Information found",
        "content": {
          "id": "10",
          "category": "Sports",
          "description":"Sports description"
        }
      }
     * 
     */
    public function api_1_0_categoryDetails() {
        if ($this->request->is(array('post','get'))) {
            if ($this->request->is('post')) {
                $requesteddata = $this->request->data;
            }
            if ($this->request->is('get')) {
                $requesteddata = $this->request->params['named'];
            }
            if ($requesteddata['id']) {
                $categoryinfo = $this->Category->find('first', array(
                    'fields' => array('id','category',
                     'description'
                    ), 
                    'conditions' => array(
                        'Category.is_active' => ACTIVE, 
                        'Category.id' => $requesteddata['id']),
                    'recursive' => -1
                        )
                );
                //print_r($categoryinfo);
                if (!empty($categoryinfo)) {
                    $status = 'SUCCESS';
                    $message = 'Category Information found';
                    $content = $categoryinfo['Category'];
                } else {
                    $status = 'ERROR';
                    $message = 'Invalid Category';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid Data';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    /**
     * @method updateCategory
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/categories/updateCategory/
     * REQUEST :
     * 
     *  {
     *      "id":10,
     *      "category":"Sports",
     *      "description":"Sports description"
     *  }
     * METHOD : PUT    
     * 
     * RESPONSE SUCCESS:
     * {
        "status": "SUCCESS",
        "message": "Category updated.",
        "content": {
          "id": 10,
          "category": "Sports",
          "description": "Sports description"
        }
      }
     * 
     */
    public function api_1_0_updateCategory() {
        if ($this->request->is('put')) {
            $requesteddata = $this->request->data;
			$this->loadModel('Category');
            $isCategoryExist = $this->Category->find('first', array('conditions' => array(
                    'Category.id' => $requesteddata['id']
            )));
            if (!empty($isCategoryExist)) {
                $this->Category->id = $isCategoryExist['Category']['id'];
                $this->Category->set($requesteddata);
                if($this->Category->validates()){
                    if ($this->Category->save($requesteddata)) {
                        $status = 'SUCCESS';
                        $message = 'Category updated.';
                        $content = $requesteddata;
                    } else {
                        $status = 'ERROR';
                        $message = 'Category update failed';
                        $content = $requesteddata;
                    }
                }else{
                    $status = 'ERROR';
                    $message = 'Data validation error.';
                    $content = $requesteddata;
                    $errors = $this->Category->validationErrors;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid Category';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        if($errors){
            $this->set([
                'status' => $status,
                'message' => $message,
                'content' => $content,
                'errors' => $errors,
                '_serialize' => ['status', 'message', 'content','errors']
            ]);
        }else{
            $this->set([
                'status' => $status,
                'message' => $message,
                'content' => $content,
                '_serialize' => ['status', 'message', 'content']
            ]);
        }
        $this->render('/' . $this->request->params['ext']);
    }
    
    
    /**
     * 
     * @method deleteCategory
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/categories/deleteCategory/
     * REQUEST :
     * 
     *  {
     *      "id":10,
     *  }
     * METHOD : DELETE    
     * 
     * RESPONSE SUCCESS:
     * 
     * {
        "status": "SUCCESS",
        "message": "Category deleted.",
        "content": {
          "id": 10
        }
      }
     * 
     * 
     */
    public function api_1_0_deleteCategory() {
        if ($this->request->is('delete')) {
            $requesteddata = $this->request->data;
            $isCategoryExist = $this->Category->find('first', array('conditions' => array(
                    'Category.id' => $requesteddata['id']
            )));
            if (!empty($isCategoryExist)) {
                $this->Category->id = $isCategoryExist['Category']['id'];
                if ($this->Category->saveField('is_active',INACTIVE)) {
                    $status = 'SUCCESS';
                    $message = 'Category deleted.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Category delete failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid Category';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
