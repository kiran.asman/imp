<?php

App::uses('ApiController', 'Api.Controller');


class CampaignStatusesController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    /**
     * @method getCategories
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/campaign_statuses/getCampaignStatuses/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * RESPONSE SUCCESS :
     * {
            "status": "SUCCESS",
            "message": "Campaign statuses found",
            "content": [
                {
                    "id": "1",
                    "status": "General"
                },
                {
                    "id": "13",
                    "status": "Games"
                }
     *          ...
     *          ...
     * 
            ],
            "pagination": {
                "page": 1,
                "current": 10,
                "count": 8,
                "prevPage": false,
                "nextPage": true,
                "pageCount": 1,
                "limit": 10,
                "paramType": "named"
            }
        }
     * 
     */
    
    public function api_1_0_getCampaignStatuses() {
        
        $this->paginate = array(
            'limit' => 10, 
              'fields' => array(
              'id',
              'status',
             // 'description'
                  ), 
            'conditions' => array(
                'CampaignStatus.is_active' => ACTIVE,
                ),
            'recursive' => -1,
            'order' => array('CampaignStatus.id' => 'desc')
        );
        $this->loadModel('CampaignStatus');
        $categoris = $this->paginate('CampaignStatus');
        $categories = Set::extract('/CampaignStatus/.', $categoris);
        if ($categories) {
            $message = 'Campaign statuses found';
            $status = 'SUCCESS';
            $content = $categories;
        } else {
            $status = 'SUCCESS';
            $message = 'Campaign statuses not found';
        }
        $pagination = $this->request->params['paging']['CampaignStatus'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    
    /**
     * @method createCategory
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/campaign_statuses/createCategory/
     * REQUEST :
     * 
     *  {
     *      "status":"Demo",
     *      "description":"Demo description.."
     *  }
     * METHOD : POST    
     * 
     * RESPONSE SUCCESS:
     *  {
            "status": "SUCCESS",
            "message": "Category created.",
            "content": {
              "id": 10,
              "status": "Demo",
              "description": "Demo description.."
            }
        }
     * 
     */
    public function api_1_0_createCampaignStatus() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;            
                
	    $this->loadModel('CampaignStatus');
            if ($this->CampaignStatus->save($requesteddata)) {
                $status = 'SUCCESS';
                $message = 'Campaign status created.';
                $content = $requesteddata;
            } else {
                $status = 'ERROR';
                $message = 'Campaign status creation failed';
                $content = $requesteddata;
            }
            
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /**
     * @method campaignStatusDetails
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/campaign_statuses/campaignStatusDetails/
     * REQUEST :
     * 
     *  {
     *      "id":10
     *  }
     * METHOD : POST
     * 
     * ====================================================================================
     * 
     *  URL: http://localhost:90/impapi/api/1.0/json/campaign_statuses/campaignStatusDetails/id:10
     * 
     *  METHOD : GET
     * 
     * RESONSE SUCCESS:
     * 
     * {
        "status": "SUCCESS",
        "message": "Campaign status Information found",
        "content": {
          "id": "10",
          "status": "Sports",
          "description":"Sports description"
        }
      }
     * 
     */
    public function api_1_0_campaignStatusDetails() {
        if ($this->request->is(array('post','get'))) {
            if ($this->request->is('post')) {
                $requesteddata = $this->request->data;
            }
            if ($this->request->is('get')) {
                $requesteddata = $this->request->params['named'];
            }
            if ($requesteddata['id']) {
                $campaignStatusinfo = $this->CampaignStatus->find('first', array(
                    'fields' => array('id','status',
                     'description',
                    ), 
                    'conditions' => array(
                        'CampaignStatus.is_active' => ACTIVE, 
                        'CampaignStatus.id' => $requesteddata['id']),
                    'recursive' => -1
                        )
                );
                //print_r($categoryinfo);
                if (!empty($campaignStatusinfo)) {
                    $status = 'SUCCESS';
                    $message = 'Campaign status information found';
                    $content = $campaignStatusinfo;
                } else {
                    $status = 'ERROR';
                    $message = 'Invalid Campaign status';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid Data';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    /**
     * @method updateCampaignStatus
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/campaign_statuses/updateCampaignStatus/
     * REQUEST :
     * 
     *  {
     *      "id":10,
     *      "status":"Sports",
     *      "description":"Sports description"
     *  }
     * METHOD : PUT    
     * 
     * RESPONSE SUCCESS:
     * {
        "status": "SUCCESS",
        "message": "Campaign status updated.",
        "content": {
          "id": 10,
          "status": "Sports",
          "description": "Sports description"
        }
      }
     * 
     */
    public function api_1_0_updateCampaignStatus() {
        if ($this->request->is('put')) {
            $requesteddata = $this->request->data;
            $isCampaignStatusExist = $this->CampaignStatus->find('first', array('conditions' => array(
                    'CampaignStatus.id' => $requesteddata['id']
            )));
            if (!empty($isCampaignStatusExist)) {
                $this->CampaignStatus->id = $isCampaignStatusExist['CampaignStatus']['id'];

                if ($this->CampaignStatus->save($requesteddata)) {
                    $status = 'SUCCESS';
                    $message = 'Campaign status updated.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Campaign status update failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid Campaign status';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    
    /**
     * 
     * @method deleteCampaignStatus
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/campaign_statuses/deleteCampaignStatus/
     * REQUEST :
     * 
     *  {
     *      "id":10,
     *  }
     * METHOD : DELETE    
     * 
     * RESPONSE SUCCESS:
     * 
     * {
        "status": "SUCCESS",
        "message": "Campaign status deleted.",
        "content": {
          "id": 10
        }
      }
     * 
     * 
     */
    public function api_1_0_deleteCampaignStatus() {
        if ($this->request->is('delete')) {
            $requesteddata = $this->request->data;
            $isCampaignStatusExist = $this->CampaignStatus->find('first', array('conditions' => array(
                    'CampaignStatus.id' => $requesteddata['id']
            )));
            if (!empty($isCampaignStatusExist)) {
                $this->CampaignStatus->id = $isCampaignStatusExist['CampaignStatus']['id'];
                if ($this->CampaignStatus->saveField('is_active',INACTIVE)) {
                    $status = 'SUCCESS';
                    $message = 'Campaign status deleted.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Campaign status delete failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid campaign status';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
