<?php

App::uses('ApiController', 'Api.Controller');

class MembersController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function api_1_0_memberSignUp() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
			$this->loadModel('User');
            $isUserExisting = $this->User->find('first', array('conditions' => array(
                    'User.user_email' => $requesteddata['user_email'])));
            if (empty($isUserExisting)) {

                if ($this->User->Save($requesteddata)) {
                    $requesteddata['user_id'] = $this->User->id;
                    $requesteddata['email'] = $requesteddata['user_email'];
                    if ($requesteddata['user_role'] === INFLUENCER) {
                        $this->loadModel('Influencer');
                        if ($this->Influencer->Save($requesteddata)) {
                            $status = 'SUCCESS';
                            $message = 'Member Registered successfully.';
                            $content = $requesteddata;
                        }
                    } else if ($requesteddata['user_role'] === AGENCY) {
                        $this->loadModel('Agency');
                        if ($this->Agency->Save($requesteddata)) {
                            $status = 'SUCCESS';
                            $message = 'Member Registered successfully.';
                            $content = $requesteddata;
                        }
                    }
                } else {
                    $status = 'ERROR';
                    $message = 'Something Went Wrong.please try again';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'User Already exits';
                $content = $this->request->data;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_memberSignIn() {

        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            //pr($requesteddata);
            if (isset($requesteddata['email']) && !empty($requesteddata['email']) && isset($requesteddata['password']) && !empty($requesteddata['password'])) {

                $email = $requesteddata['email'];
                $password = $requesteddata['password'];
                $hashedpwd = AuthComponent::password($password);
                //print_r($hashedpwd);
                $this->loadModel('User');
                $options = array(
                    'fields' => array(
                        'Agency.id',
                        'Agency.email',
                        'Agency.first_name',
                        'Agency.last_name',
                        'Agency.brand_name',
                        'Agency.gender',
                        'Agency.designation',
                        'Agency.phone_no',
                        'Agency.city',
                        'Agency.logo',
                        'Admin.id',
                        'Admin.email',
                        'Admin.first_name',
                        'Admin.last_name',
                        'Admin.gender',
                        'Admin.designation',
                        'Admin.phone_no',
                        'Admin.city',
                        'Admin.profile_pic',
                        'Influencer.id',
                        'Influencer.email',
                        'Influencer.first_name',
                        'Influencer.last_name',
                        'Influencer.dob',
                        'Influencer.gender',
                        'Influencer.phone_no',
                        'Influencer.city',
                        'Influencer.photo',
                        'Influencer.alternate_number',
                        'Influencer.address',
                        'Influencer.state',
                        'Influencer.country',
                        'User.id',
                        'User.is_active',
                        'User.user_email',
                        'User.user_role',
                    ),
                    'conditions' => array(
                        'User.is_active' => ACTIVE,
                        'User.user_email' => $email,
                        'User.pswd' => $hashedpwd),
                    'recursive' => 0,
                );
                //pr($options);
                $userdata = $this->User->find('first', $options);
                //pr($userdata);                
                if (!$userdata) {
                    //throw new UnauthorizedException('Invalid username or password');
                    $status = 'ERROR';
                    $message = 'Invalid loginid OR password.';
                    $content = $requesteddata;
                } else {

                    //print_r($userdata);
                    $memberid = null;
                    $profile['profile'] = null;
                    if ($userdata['User']['user_role'] === AGENCY) {
                        $memberid = $userdata['Agency']['id'];
                        $profile['profile'] = $userdata['Agency'];
                    } elseif ($userdata['User']['user_role'] === INFLUENCER) {
                        $memberid = $userdata['Influencer']['id'];
                        $profile['profile'] = $userdata['Influencer'];
                    } elseif ($userdata['User']['user_role'] === ADMIN) {
                        $memberid = 0;
                    }
                    $userid = $userdata['User']['id'];
                    $emailid = $userdata['User']['user_email'];
                    $memberrole = $userdata['User']['user_role'];
                    $token = $this->generateToken($userid, $memberid, $emailid, $memberrole);

                    $status = 'SUCCESS';
                    $message = 'Login success.';
                    $requesteddata['token'] = $token;

                    $profile['profile']['member_id'] = $memberid;
                    $profile['profile']['member_type'] = $memberrole;
                    $profile['profile']['user_id'] = $userid;
                    $profile['profile']['email'] = $emailid;
                    $requesteddata['profile'] = $profile['profile'];
                    //$requesteddata['member_type'] = $memberrole;
                    //$requesteddata['user_id'] = $userid;
                    //$requesteddata['member_id'] = $memberid;
                    $requesteddata['user_type'] = $memberrole;
                    unset($requesteddata['profile']['id']);
                    unset($requesteddata['email']);
                    unset($requesteddata['password']);
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid request.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Bad request.';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_getInfluencers() {
        $this->paginate = array(
            'limit' => 10,
            'fields' => array(
                'id',
                'first_name',
                'phone_no',
                'bio',
            // 'description'
            ),
            'conditions' => array(
             'User.is_active' => 1,
            ),
            'recursive' => 1,
                //'order' => array('Influencer.id' => 'desc')
        );
        $this->loadModel('Influencer');
        $influencers = $this->paginate('Influencer');
        $influencers = Set::extract('/Influencer/.', $influencers);
        if ($influencers) {
            $message = 'Records found';
            $status = 'SUCCESS';
            $content = $influencers;
        } else {
            $status = 'SUCCESS';
            $message = 'Records not found';
        }
        //pr($influencers);
        $pagination = $this->request->params['paging']['Influencer'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
    public function api_1_0_logout() {
        
        $jwtsettings = $this->Auth->authenticate['JwtToken.JwtToken'];

        $token = $this->request->header($jwtsettings['header']);
        
        if ($token) {
            App::import('Vendor', 'JWT' . DS . 'JWT');
            $tokeninfo = JWT\JWT::decode($token, $jwtsettings['pepper'], array('HS256'));
            
            if ($tokeninfo) {
                $this->loadModel('User');
                $result = $this->User->findByIdAndAuthenticationCode($tokeninfo->user_id, $tokeninfo->authentication_code);
                $this->log($result, 'debug');
                if ($this->User->updateAll(array('authentication_code' => null), array('User.id' => $tokeninfo->user_id))) {
                    $status = "SUCCESS";
                    $message = "Logout success.";
                    $content = null;
                } else {
                    $status = "ERROR";
                    $message = "Logout failed.";
                    $content = null;
                }
            } else {
                $status = "ERROR";
                $message = "Invalid token";
                $content = null;
            }
        } else {
            $status = "ERROR";
            $message = "Invalid token";
            $content = null;
        }

        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
