<?php

/**
 *
 * PHP 5
 *
 * @company 	Fingersfun 
 * @link 		www.fingersfun.com 
 * @version 	Version 0.1.0 
 * @developer 	Kiran Kumar.M (kiran@fingersfun.com) 
 *
 **/

App::uses('AppController', 'Controller');
/**
 * Pages Controller
 *
 * @property Page $Page
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 **/
class PagesController extends AppController {
	/**
	 * Components
	 *
	 * @var array
	 **/
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * admin_index method
 *
 * @return void
 **/
	public function admin_index() {
		$this->Page->recursive = 0;
		$pages = $this->paginate();
		$this->set(compact('pages'));
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 **/
	public function admin_view($id = null) {
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid page'));
		}
		$conditions = array('Page.id' => $id);
		$page = $this->Page->find('first', compact('conditions'));
		$this->set(compact('page'));
	}

/**
 * admin_add method
 *
 * @return void
 **/
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Page->create();
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been saved'),'Flash/success');
				return $this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'),'Flash/error');
			}
		}
		$parentPages = $this->Page->ParentPage->find('list');
		$this->set(compact('parentPages'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 **/
	public function admin_edit($id = null) {
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been saved'),'Flash/success');
                return $this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'),'Flash/error');
			}
		} else {
            $conditions = array('Page.id'=>$id);
            $page = $this->Page->find('first', compact('conditions'));
			$this->request->data = $page;
		}
		$parentPages = $this->Page->ParentPage->find('list');
		$this->set(compact('parentPages'));
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 **/
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->Page->delete()) {
			$this->Session->setFlash(__('Page deleted'),'Flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Page was not deleted'),'Flash/error');
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_deactivate method
 *
 * @param string $id
 * @return void
 **/
	public function admin_deactivate($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid Page'));
		}
		
		if ($this->Page->updateAll(array('is_active'=>0),array('Page.id'=>$id))) {
			$this->Session->setFlash(__('Page deactivated'),'Flash/success');
			return $this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Page was not deactivated'),'Flash/error');
		return $this->redirect($this->referer());
	}

/**
 * admin_activate method
 *
 * @param string $id
 * @return void
 **/
	public function admin_activate($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid Page'));
		}
		
		if ($this->Page->updateAll(array('is_active'=> 1),array('Page.id'=>$id))) {
			$this->Session->setFlash(__('Page activated'),'Flash/success');
			return $this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Page was not activated'),'Flash/error');
		return $this->redirect($this->referer());
	}
}
