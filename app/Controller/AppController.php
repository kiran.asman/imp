<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(       
        'RequestHandler',
        'Auth',
    );
   
    public function beforeFilter() {

        // Allow from any origin
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            // should do a check here to match $_SERVER['HTTP_ORIGIN'] to a
            // whitelist of safe domains
            //header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            //header('Access-Control-Allow-Credentials: true');
            //header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        }
                 
        $this->Auth->allow();
        //$this->Auth->allow('api_1_0_memberSignIn','api_1_0_memberSignUp');
        
        $JWTSignature = Configure::read('JWTSignature');
        $this->Auth->actionPath = 'controllers';
        
        $this->Auth->authenticate = array(
            
            'JwtToken.JwtToken' => array(
                'fields' => array(
                    'username' => 'user_email',
                    'password' => 'pswd',
                    'token' => 'authentication_code',
                ),
                'header' => 'X-JWT-AUTH',
                'userModel' => 'User',
                'scope' => array(
                    'User.is_active' => ACTIVE
                ),
                'recursive' => 0,
                'query_parameter' => 'auth_token',
                'data_parameter' => 'auth_token',
                'pepper' => $JWTSignature,
            )
        );
        //pr($this->Auth->authenticate);
        parent::beforeFilter();
        
        $this->response->cors(
            $this->request,
            [$this->request->host()],
            ['PUT', 'GET', 'POST', 'DELETE', 'OPTIONS'],
            ['origin', 'X-JWT-AUTH', 'content-type'],
            true
        );        
    }

    public function isAuthorized($user) {
        // Here is where we should verify the role and give access based on role
        if ($this->Auth->user()) {
            return true;
        }
        return false;
    }
    
    public function getTokenInfo() {

        $jwtsettings = $this->Auth->authenticate['JwtToken.JwtToken'];

        $token = $this->request->header($jwtsettings['header']);

        if ($token) {
            App::import('Vendor', 'JWT' . DS . 'JWT');
            $tokeninfo = JWT\JWT::decode($token, $jwtsettings['pepper'], array('HS256'));            
            //pr($tokeninfo);
            if ($tokeninfo) {
                $tokendata['user_id'] = $tokeninfo->user_id;
                $tokendata['member_id'] = $tokeninfo->member_id;
                $tokendata['member_type'] = $tokeninfo->member_type;
                return $tokendata;
            } else {
                return false;
            }
        }
    }

    public function generateToken($userid,$memberid, $emailid,$memberrole) {

        //new random token
        $newtoken = sha1(CakeText::uuid());

        //prepare token data
        $usetken['user_id'] = $userid;
        $usetken['member_id'] = $memberid;
        $usetken['user_email'] = $emailid;
        $usetken['member_type'] = $memberrole;
        $usetken['authentication_code'] = $newtoken;

        //encode token
        $JWTSignature = Configure::read('JWTSignature');
        App::import('Vendor', 'JWT' . DS . 'JWT');
        $token = JWT\JWT::encode($usetken, $JWTSignature);

        //save token
        $this->loadModel('User');
        $this->User->id = $userid;
        $this->User->saveField('authentication_code', $newtoken);

        return $token;
    }

    public function sendMail($messaage_body, $subject, $template, $recipient_name, $recipient_email) {
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail('smtp_imp_mail');
        //$email->from(array($sender_email => $sender_name));
        $email->to(array($recipient_email => $recipient_name));
        $email->subject($subject);
        $email->emailFormat('both'); // both = html + text.        
        $email->template($template);
        //$message = 'Hi,' . $recipient_name . ', Your SecureID Code is: ' . $pass;
        if ($email->send($messaage_body)) {
            //echo "Message has been sent.";
            return true;
        } else {
            echo $email->smtpError;
            //return false;
        }
    }

    public function generateStr($length = 6, $add_dashes = false, $available_sets = 'luds') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }
    /*
    public function getTokenInfo() {
        //App::uses('AuthComponent', 'Controller/Component');
        //App::uses('JwtToken.JwtToken', 'Controller/Component/Auth');
        //return JwtTokenAuthenticate::authenticate();
        return AuthComponent::authenticate();
    }
    */
}
