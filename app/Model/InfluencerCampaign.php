<?php
App::uses('AppModel', 'Model');
/**
 * InfluencerCampaign Model
 *
 * @property Influencer $Influencer
 * @property Campaign $Campaign
 * @property CampaignStatus $CampaignStatus
 */
class InfluencerCampaign extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'is_read' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'campaign_status_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Influencer' => array(
			'className' => 'Influencer',
			'foreignKey' => 'influencer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Campaign' => array(
			'className' => 'Campaign',
			'foreignKey' => 'campaign_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CampaignStatus' => array(
			'className' => 'CampaignStatus',
			'foreignKey' => 'campaign_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
